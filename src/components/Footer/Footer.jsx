import React from "react";
import "./Footer.css";
import Wave from "../../img/wave.png";
import Github from "https://mpng.subpng.com/20180824/jtl/kisspng-computer-icons-logo-portable-network-graphics-clip-icons-for-free-iconza-circle-social-5b7fe46b0bac53.1999041115351082030478.jpg";
import Facebook from "https://image.similarpng.com/very-thumbnail/2021/01/Facebook-icon-design-on-transparent-background-PNG.png";
import LinkedIn from "https://w7.pngwing.com/pngs/887/616/png-transparent-linkedin-icon-linkedin-text-rectangle-logo.png";

const Footer = () => {
  return (
    <div className="footer">
      <img src={Wave} alt="" style={{ width: "100%" }} />
      <div className="f-content">
        <span>deanzdeluxe@gmail.com</span>
        <div className="f-icons">
         <a href="https://github.com/DeanRodel">
          <Github color="white" size={"3rem"} />
          </a>
          <a href="https://web.facebook.com/deanz.legada.9">
          <Facebook color="white" size={"3rem"} />
          </a>
           <a href="https://www.linkedin.com/in/dean-rodel-legada-12b325197">
          <LinkedIn color="white" size={"3rem"} />
          </a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
